require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
//const url = 'mongodb://localhost/userManagement'

const app = express();

app.use(express.json());
app.use('/uploads',express.static('uploads'))

mongoose.connect(process.env.DATABASE_URL)
const con = mongoose.connection

con.on('open',()=>{
    console.log('connected..');
})

const signUp = require('./routes/signup')
app.use('/signup', signUp)

const userList = require('./routes/list')
app.use('/list', userList)

const userLogin = require('./routes/login')
app.use('/login', userLogin)

const updateUser = require('./routes/update')
app.use('/update', updateUser)

const deleteUser = require('./routes/delete')
app.use('/delete', deleteUser)

const findByName = require('./routes/findByName')
app.use('/find', findByName)



app.listen(9000,()=>{ 
    console.log('server started');
})


