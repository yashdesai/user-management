const express = require('express');
const router = express.Router()
const User = require('../models/user')  

router.put('/:id', async (req,res)=>{
    try{
        const user = await User.findById(req.params.id)
        user.email = req.body.email,
        user.name = req.body.name,
        user.address = req.body.address,
        user.phone = req.body.phone,
        user.gender = req.body.gender,
        user.dob = req.body.dob,
        user.category = req.body.category,
        user.password = req.body.name + '@sa'

        const u = await user.save()
        res.json(u)
    }catch(err){
        res.send('error')
        console.log(err.message)
    }
})


module.exports = router;