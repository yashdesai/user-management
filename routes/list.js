const express = require('express');
const router = express.Router()
const User = require('../models/user')  

//getting all user
router.get('/',async (req,res)=>{
    try{
         const users = await User.find()
         res.json(users)
    }catch{
        res.send('Error' + err)
    }
})

//getting one user
router.get('/:id',async (req,res)=>{
    try{
         const users = await User.findById(req.params.id)
         const { password , created , ...others} = users._doc;
         res.send({...others});
    }catch{
        res.send('Error' + err)
    }
})


module.exports = router;

