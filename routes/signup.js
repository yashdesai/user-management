const express = require("express");
const router = express.Router();
const User = require("../models/user");
const multer = require('multer');

//adding path of the uploaded file in the schema
const storage = multer.diskStorage({
  destination: (req, file, callback)=>{
    callback(null, './uploads/')
  },
  filename: (req, file, callback)=>{
    callback(null,file.originalname);
  }
})


const upload = multer({storage: storage})

router.post("/", upload.single('profileImage'),async (req, res) => {

  // console.log(req.file);
  // console.log(req.body);
  try {
    let email = req.body.email;
    let enteredEmail = await User.findOne({ email: email });

    if (enteredEmail) {
      return res.json({ message: "User already exists." });
    } else {
      const user = new User({
        email: req.body.email,
        name: req.body.name,
        address: req.body.address,
        phone: req.body.phone,
        gender: req.body.gender,
        dob: req.body.dob,
        category: req.body.category,
        created: new Date().toString(),
        password: req.body.name + "@sa",
        profileImage: req.file.path
      });

      const u1 = await user.save();
      console.log(u1);
      res.send(u1);
      // return res.json({ message: "User resgistered successfully." });
    }
  } catch (err) {
    return res.json({ message: "Registration Unsuccessfull." });
  }
});

module.exports = router;
