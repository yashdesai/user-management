const express = require('express');
const router = express.Router()
const User = require('../models/user')  

router.post('/', async (req,res)=>{

    try{
        const email = req.body.email
        const password = req.body.password

        const loginEmail = await User.findOne({
            email: email
        })

        if(loginEmail.password === password){
            console.log('Login success');
            res.json({message:"Logged in"})
        }else{
            res.json({message:"Incorrect password"})
        }
    }catch(err){
        res.json({message:"Invalid login details"})
    }
})


module.exports = router;