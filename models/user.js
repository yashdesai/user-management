const mongoose = require('mongoose');
require('mongoose-type-email')

const userSchema = new mongoose.Schema({

      email:{
          type: mongoose.SchemaTypes.Email,
          required: true,
          correctTld: true
      },
      name:{
          type: String,
          required: true,
      },
      address:{
            type: String,
            required:true,
      },
      phone:{
            type: Number,
            required: true,
            min: 1000000000,
            max:9999999999
      },
      gender:{
          type:String,
          required: true
      },
      dob:{
          type: String,
          required: true
      },
      category:{
          type: String,
          required: true
      },
      created:{
          type:String,
          timestamps:true
      },
      password:{
          type:String,
          required: true
      },
      profileImage:{
          type:String,
          required: true
      }
})

module.exports = mongoose.model('User',userSchema)